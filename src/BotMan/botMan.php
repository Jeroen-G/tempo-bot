<?php

declare(strict_types=1);

include __DIR__ . '/../vendor/autoload.php';

use BotMan\BotMan\BotMan;
use Carbon\CarbonImmutable;
use Predis\Client;
use TempoBot\BotMan\BotMan\BotManFactory;
use TempoBot\BotMan\Commands\HelpCommand;
use TempoBot\BotMan\Commands\ReportCommand;
use TempoBot\BotMan\Commands\WelcomeMessageCommand;
use TempoBot\BotMan\Conversations\UpdateConfigConversation;
use TempoBot\BotMan\Repositories\RedisWelcomeMessageRepository;
use TempoBot\Config\Config;
use TempoBot\EnvVars\EnvVars;
use TempoBot\Tempo\Client\ClientFactory;
use TempoBot\Tempo\Services\CachedGetWorklogsService;
use TempoBot\Tempo\Services\GetWorklogsService;

EnvVars::init();


$botMan = BotManFactory::buildWithHardcodedConfig();

///// Configuration setting commands /////

$botMan->hears('config update', function (BotMan $bot) {
    $bot->startConversation(new UpdateConfigConversation());
});


///// Core functionality commands /////

$botMan->hears(ReportCommand::getCommandString(), function (BotMan $bot) {
    /** @var Config $config */
    $config = $bot->getMessage()->getExtras(Config::class);

    $worklogsService = new GetWorklogsService(
        (new ClientFactory(
            $config->getTempoAccessToken()
        ))->build()
    );

    $redisClient = new Client([
        'host' => $_SERVER['REDIS_HOSTNAME'],
    ]);

    $cachedWorklogsService = new CachedGetWorklogsService(
        $worklogsService,
        $redisClient
    );

    $baseDate = CarbonImmutable::now('UTC')->subWeeks(0);
    $bot->reply((new ReportCommand($cachedWorklogsService))->handle(
        $config,
        $baseDate->subWeeks(2)->startOfDay(),
        $baseDate->endOfDay()
    ));
});


///// Help commands /////

$botMan->hears('help', function (BotMan $bot) {
    $bot->reply((new HelpCommand())->handle([]));
});

$botMan->hears(HelpCommand::getCommandString(), function (BotMan $bot, string $subject) {
    $bot->reply((new HelpCommand())->handle([
        'subject' => $subject
    ]));
});


///// Misc. infrastructure plumbing /////

$botMan->on(WelcomeMessageCommand::getCommandString(), function ($payload, BotMan $bot) {
    $command = new WelcomeMessageCommand(new RedisWelcomeMessageRepository());
    $command->handle($bot);
});

$botMan->hears('cancel', function (BotMan $bot) {
    $bot->reply('Cancelled');
})->stopsConversation();

$botMan->fallback(function (BotMan $bot) {
    $bot->reply('I did not understand what you just said, to get help please type `help`.');
});

// Start listening
$botMan->listen();
