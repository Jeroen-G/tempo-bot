<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Commands;

class HelpCommand implements CommandInterface
{
    static $fallbackHelpText = 'No help text available, please use `help` for a list of subjects';

    public static function getCommandString(): string
    {
        return 'help (.*)';
    }

    public function handle(array $params = []): string
    {
        $subject = trim($params['subject'] ?? '');

        switch ($subject) {
            case 'about':
                return $this->getText('about');

            case 'config distribution':
                return $this->getText('config/distribution');

            case 'config invalid':
                return $this->getText('config/invalid');

            case 'config update':
                return $this->getText('config/update');

            case 'tempo setup':
                return $this->getText('tempo/setup');

            case 'report':
                return $this->getText('report');

            default:
                return $this->getText('index');
        }
    }

    private function getText(string $filename): string
    {
        $path = sprintf('%s/BotMan/texts/help/%s.md', $_SERVER['project_root'], $filename);

        if (is_readable($path)) {
            return file_get_contents($path);
        }

        return self::$fallbackHelpText;
    }
}
