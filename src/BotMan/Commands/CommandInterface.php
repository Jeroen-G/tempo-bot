<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Commands;

interface CommandInterface
{
    public static function getCommandString(): string;
}
