<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use Exception;
use Predis\Client;
use TempoBot\Config\Config;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\Exception\InputEmptyException;
use TempoBot\Config\Exception\InputInvalidJSONException;
use TempoBot\Config\Exception\MessageIsUserReadable;
use TempoBot\Config\Repository\RedisConfigRepository;

class UpdateConfigConversation extends Conversation
{
    public function run()
    {
        /** @var Config $config */
        $config = $this->getBot()->getMessage()->getExtras(Config::class);
        $this->say("Your current config is :\n```{$config->asPrettyJson()}```");
        $this->askForConfig();
    }

    public function askForConfig()
    {
        $this->ask(
            'Please update it and respond with your desired config (or with "cancel" to cancel).',
            function (Answer $answer) {
                $configFactory = new ConfigFactory();
                $configRepository = new RedisConfigRepository(
                    new Client([
                        'host' => $_SERVER['REDIS_HOSTNAME'],
                    ]),
                    $configFactory
                );

                $answerText = $this->getAnswerText($answer);
                try {
                    $config = $configFactory->build($answerText);

                    $configRepository->createOrUpdate($this->getBot()->getMessage()->getSender(), $config);
                    $this->getBot()->getMessage()->addExtras(Config::class, $config);

                    $this->say("Your config was successfully updated to:\n```{$config->asPrettyJson()}```");

                } catch (InputInvalidJSONException $e) {
                    $this->say('Updating your config FAILED because it was INVALID JSON');
                    $this->askForConfig();

                } catch (InputEmptyException $e) {
                    $this->say('Updating your config FAILED because it was EMPTY');
                    $this->askForConfig();

                } catch (MessageIsUserReadable $e) {
                    $this->say(sprintf('Updating your config FAILED because: "%s"', $e->getMessage()));
                    $this->askForConfig();

                } catch (Exception $e) {
                    $this->say(sprintf('Updating your config FAILED because: "%s"', $e->getMessage()));
                    $this->askForConfig();
                }
            }
        );
    }

    private function getAnswerText(Answer $answer): string
    {
        $answerText = $answer->getText();

        if ($this->textIsWrappedInCodeBlock($answerText)) {
            $answerText = $this->removeCodeBlockFormatting($answerText);
        }

        return trim($answerText);
    }

    private function textIsWrappedInCodeBlock(string $answerText): bool
    {
        return strpos($answerText, '```') === 0;
    }

    private function removeCodeBlockFormatting(string $answerText): string
    {
        $answerText = substr($answerText, 3, -4);
        $answerText = html_entity_decode($answerText);
        $answerText = str_replace("\n", '', $answerText);
        $answerText = str_replace("\xC2\xA0", '', $answerText);

        return $answerText;
    }
}
