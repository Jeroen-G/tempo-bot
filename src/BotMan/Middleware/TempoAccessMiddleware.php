<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\Middleware\Received;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use TempoBot\BotMan\Commands\ReportCommand;
use TempoBot\Config\Config;
use TempoBot\Tempo\Client\Client;
use TempoBot\Tempo\Client\ClientFactory;

class TempoAccessMiddleware implements Received
{
    public function received(IncomingMessage $message, $next, BotMan $bot)
    {
        /** @var Config $config */
        $config = $message->getExtras(Config::class);

        if (!$config instanceof Config) {
            if ($this->accessToTempoIsNeeded($message)) {
                $message->setText('help config invalid');
            }
            return $next($message);
        }

        $tempoClient = (new ClientFactory($config->getTempoAccessToken()))->build();

        if ($tempoClient instanceof Client) {
            $message->addExtras(Client::class, $tempoClient);

        } elseif ($this->accessToTempoIsNeeded($message)) {
            $message->setText('help config invalid');
        }

        return $next($message);
    }

    private function accessToTempoIsNeeded(IncomingMessage $message)
    {
        return in_array($message->getText(), [
            ReportCommand::getCommandString(),
        ]);
    }
}
