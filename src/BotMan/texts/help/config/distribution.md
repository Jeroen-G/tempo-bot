*config distribution*

*Worklog*
This is the name Tempo gives to each separate period of time you log in it. Each *worklog* is linked to a Jira issue via its key (e.g. "FOO-14"), which I use during the matching process.


*Distribution*
Part of your config is the *distribution*, which is the way you tell me how you intend to distribute your time on average. It consists of one or more *buckets*, and each *worklog* I retrieve from Tempo will be put into one of them when generating a report.


*Bucket*
A *bucket* has three properties:
* *label*: the name I will use for this bucket
* *matchers*: an array of one or more strings, each containing a regular expression
* *plannedHours*: an integer with the average total amount of hours you want to spend on this bucket in any 14 day period


*Matcher*
As mentioned, *matchers* are regular expressions that I will try to match on the key of the Jira issue that a *worklog* is linked to. Usually these can be pretty simple: "FOO-" to match all issues from the "FOO" project, and sometimes you might want to use "FOO-14" to only match a specific issue. But feel to go as regex-mad as you like :-).


*Matching order*
Note that I will try to match each *worklog* on each bucket in your config, from top to bottom, and stop once I have found a match (or I have tried all *buckets*). That means that the order of your *buckets* in your *distribution* is important: put specific *buckets* on top and more generic ones on the bottom.
In fact: if I cannot match a *worklog* on any *buckets*, I will not add it to any of them. I will however, still count it for the "Total" row at the bottom.

I therefore strongly advise you to put a "Remainder" *bucket* with a `.*` *matcher* and 0 *plannedHours* on the bottom of your distribution, to keep an eye on hours logged on unexpected projects. 
