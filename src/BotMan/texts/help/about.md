*About*
I was created by Johan van Overbeeke, out of a desire to have more insight into (and therefore: control over) what he spends his time on.
Project homepage: https://gitlab.com/instant-project/tempo-bot/


*Privacy*
Johan has to be very selective what he spends his time on, so actively invading your privacy is way too much effort. On the other hand: don't expect the highest standards of anonymity/security either. When in doubt, read the source yourself.

tl;dr: *USE AT YOUR OWN RISK*, as the creator takes 0 responsibility
