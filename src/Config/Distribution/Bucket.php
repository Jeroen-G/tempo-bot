<?php

declare(strict_types=1);

namespace TempoBot\Config\Distribution;

use JsonSerializable;
use stdClass;
use TempoBot\Config\Exception\InputInvalidException;

class Bucket implements JsonSerializable
{
    /** @var string */
    private $label;

    /** @var BucketMatcher[] */
    private $matchers = [];

    /** @var int */
    private $plannedHours;

    /** @var int */
    private $workedSeconds;

    /**
     * @param string $label
     * @param array  $matchers
     * @param int    $plannedHours
     * @param int    $workedSeconds
     *
     * @throws InputInvalidException
     */
    public function __construct(string $label, array $matchers, int $plannedHours = 0, int $workedSeconds = 0)
    {
        $this->setLabel($label);

        foreach ($matchers as $pattern) {
            $this->addMatcher(new IssueKeyRegexMatcher($pattern));
        }
        if (count($this->matchers) <= 0) {
            throw new InputInvalidException('Buckets must have at least one thing to match on');
        }
        $this->setPlannedHours($plannedHours);
        $this->setWorkedSeconds($workedSeconds);
    }

    private function setLabel(string $label): void
    {
        if (strlen(trim($label)) <= 1) {
            throw new InputInvalidException("Label of of bucket is empty");
        }

        $this->label = $label;
    }

    public function addMatcher($matcher): void
    {
        if (!$matcher instanceof BucketMatcher) {
            $matcherType = 'NULL';
            if (!is_null($matcher)) {
                $matcherType = is_object($matcher) ? get_class($matcher) : 'not an object';
            }

            throw new InputInvalidException(
                "Matcher of bucket {$this->getLabel()} is unusable, as its (a) '{$matcherType}'."
            );
        }

        $this->matchers[] = $matcher;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    private function setPlannedHours(int $plannedHours): void
    {
        if ($plannedHours < 0) {
            throw new InputInvalidException(
                "Planned hours for matcher '{$this->getLabel()}' cannot be less than zero"
            );
        }

        $this->plannedHours = $plannedHours;
    }

    public static function buildFromDecodedJson(stdClass $input)
    {
        return new self(
            $input->label ?? '',
            $input->matchers ?? [],
            $input->plannedHours ?? 0,
            $input->workedSeconds ?? 0
        );
    }

    public function getWorkedHours(): float
    {
        return ($this->workedSeconds / (60 * 60));
    }

    public function addToWorkedSeconds(int $seconds): void
    {
        $this->setWorkedSeconds($this->getWorkedSeconds() + $seconds);
    }

    public function getWorkedSeconds(): int
    {
        return $this->workedSeconds;
    }

    public function setWorkedSeconds(int $workedSeconds): void
    {
        if ($workedSeconds < 0) {
            throw new InputInvalidException(
                "Worked seconds for matcher '{$this->getLabel()}' cannot be less than zero"
            );
        }

        $this->workedSeconds = $workedSeconds;
    }

    public function jsonSerialize(): stdClass
    {
        $output = new stdClass();
        $output->label = $this->getLabel();
        $output->matchers = [];
        foreach ($this->getMatchers() as $matcher) {
            $output->matchers[] = $matcher->jsonSerialize();
        }
        $output->plannedHours = $this->getPlannedHours();

        return $output;
    }

    public function getMatchers(): array
    {
        return $this->matchers;
    }

    public function getPlannedHours(): int
    {
        return $this->plannedHours;
    }
}
