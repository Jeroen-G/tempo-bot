<?php

declare(strict_types=1);

namespace TempoBot\Config;

use TempoBot\Config\Distribution\Bucket;

final class EmptyConfig extends BaseConfig
{
    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        $this->tempoAccountId = '';
        $this->tempoAccessToken = '';
        $this->setDistribution([
            new Bucket(
                'bucketLabel',
                [
                    '.*'
                ],
                1
            )
        ]);
    }
}
