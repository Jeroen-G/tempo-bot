<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Client;

use Carbon\CarbonImmutable;
use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;

class Client
{
    /* @var GuzzleClient */
    private $guzzleClient;

    /* @var string */
    private $token;

    public function __construct(GuzzleClient $guzzleClient, string $token)
    {
        $this->guzzleClient = $guzzleClient;
        $this->token = $token;
    }

    public function canConnect(): bool
    {
        $response = $this->request('worklogs', [
            'from' => CarbonImmutable::createFromTimestamp(0)->toDateString(),
            'to' => CarbonImmutable::now()->toDateString(),
            'limit' => 1,
        ]);

        return isset($response)
            && is_object($response)
            && $response instanceof ResponseInterface
            && $response->getStatusCode() === 200;
    }

    public function request(string $uri, array $queryParams): ResponseInterface
    {
        return $this->guzzleClient->get($uri, [
            'headers' => [
                'Authorization' => "Bearer {$this->token}"
            ],
            'query' => $queryParams
        ]);
    }
}
