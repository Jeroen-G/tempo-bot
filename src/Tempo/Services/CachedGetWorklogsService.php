<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Services;

use Carbon\CarbonImmutable;
use Predis\Client;
use stdClass;
use TypeError;

final class CachedGetWorklogsService implements GetWorklogsServiceInterface
{
    private const RETRIEVED_DATE_FORMAT = 'Y-m-d H:i:s';

    /** @var GetWorklogsService */
    private $getWorklogsService;

    /** @var Client */
    private $redisClient;

    public function __construct(
        GetWorklogsService $getWorklogsService,
        Client $redisClient
    )
    {
        $this->getWorklogsService = $getWorklogsService;
        $this->redisClient = $redisClient;
    }

    public function getWorklogs(
        string $accountId,
        CarbonImmutable $from,
        CarbonImmutable $to
    ): array
    {
        $cacheKey = "tempo-bot:cache:get-worklogs:{$accountId}";

        $cacheValue = $this->retrieveCacheValue($cacheKey);
        if (is_object($cacheValue) && $this->cacheValueIsFresh($cacheValue)) {
            return $cacheValue->content;
        }

        $freshCacheValue = $this->retrieveFreshWorklogs($accountId, $from, $to);

        $this->persistCacheValue($cacheKey, $freshCacheValue);

        return $freshCacheValue->content;
    }

    protected function retrieveCacheValue(string $cacheKey): ?stdClass
    {
        $cacheValue = $this->redisClient->get($cacheKey);

        try {
            $decodedCacheValue = unserialize($cacheValue);
        } catch (TypeError $e) {
            $decodedCacheValue = null;
        }

        return $decodedCacheValue;
    }

    protected function cacheValueIsFresh(stdClass $cacheValue): bool
    {
        return isset($cacheValue->retrievedAt)
            && CarbonImmutable::now()
                ->subMinutes(5)
                ->isBefore(CarbonImmutable::createFromFormat(self::RETRIEVED_DATE_FORMAT, $cacheValue->retrievedAt));
    }

    protected function retrieveFreshWorklogs(string $accountId, CarbonImmutable $from, CarbonImmutable $to): stdClass
    {
        $freshWorklogs = $this->getWorklogsService->getWorklogs($accountId, $from, $to);

        $freshCacheValue = new stdClass();
        $freshCacheValue->retrievedAt = CarbonImmutable::now()->format(self::RETRIEVED_DATE_FORMAT);
        $freshCacheValue->content = $freshWorklogs;

        return $freshCacheValue;
    }

    /**
     * @param string   $cacheKey
     * @param stdClass $freshCacheValue
     */
    protected function persistCacheValue(string $cacheKey, stdClass $freshCacheValue): void
    {
        $this->redisClient->set($cacheKey, serialize($freshCacheValue));
    }
}
