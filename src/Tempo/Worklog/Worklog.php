<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Worklog;

use Carbon\CarbonImmutable;
use DateTimeZone;
use InvalidArgumentException;
use stdClass;
use Throwable;

class Worklog
{
    /** @var int */
    private $tempoWorklogId;

    /** @var string */
    private $issueKey;

    /** @var string */
    private $description;

    /** @var int */
    private $timeSpentSeconds;

    /** @var int */
    private $billableSeconds;

    /** @var CarbonImmutable */
    private $start;

    /** @var CarbonImmutable */
    private $created;

    /** @var CarbonImmutable */
    private $updated;

    public function __construct(
        int $tempoWorklogId,
        string $issueKey,
        string $description,
        int $timeSpentSeconds,
        int $billableSeconds,
        CarbonImmutable $start,
        CarbonImmutable $created,
        CarbonImmutable $updated
    )
    {
        $this->tempoWorklogId = $tempoWorklogId;
        $this->setIssueKey($issueKey);
        $this->description = $description;
        $this->timeSpentSeconds = $timeSpentSeconds;
        $this->billableSeconds = $billableSeconds;
        $this->start = $start;
        $this->created = $created;
        $this->updated = $updated;
    }

    private function setIssueKey(string $issueKey): void
    {
        if (!trim($issueKey)) {
            throw new InvalidArgumentException('Issue key may not be empty');
        }
        $this->issueKey = $issueKey;
    }

    /**
     * @param stdClass $input
     *
     * @throws InvalidArgumentException
     * @return Worklog
     */
    public static function fromApiOutput(stdClass $input): Worklog
    {
        try {
            return new self(
                $input->tempoWorklogId,
                $input->issue->key,
                $input->description,
                $input->timeSpentSeconds,
                $input->billableSeconds,
                self::buildDatetime("{$input->startDate}T{$input->startTime}Z"),
                self::buildDatetime($input->createdAt),
                self::buildDatetime($input->updatedAt),
            );
        } catch (Throwable $throwable) {
            throw new InvalidArgumentException(
                sprintf(
                    'Got "%s" with this message "%s"',
                    get_class($throwable),
                    $throwable->getMessage()
                )
            );
        }
    }

    private static function buildDatetime(string $dateTimeString): CarbonImmutable
    {
        $utcTimezone = new DateTimeZone('UTC');
        $dutchTimezone = new DateTimeZone('Europe/Amsterdam');

        return CarbonImmutable::createFromFormat(
            'Y-m-d\TH:i:s\Z',
            $dateTimeString,
            $utcTimezone
        )->setTimezone($dutchTimezone);
    }

    public function getTempoWorklogId(): int
    {
        return $this->tempoWorklogId;
    }

    public function getIssueKey(): string
    {
        return $this->issueKey;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getTimeSpentSeconds(): int
    {
        return $this->timeSpentSeconds;
    }

    public function getBillableSeconds(): int
    {
        return $this->billableSeconds;
    }

    public function getStart(): CarbonImmutable
    {
        return $this->start;
    }

    public function getCreated(): CarbonImmutable
    {
        return $this->created;
    }

    public function getUpdated(): CarbonImmutable
    {
        return $this->updated;
    }
}
