#!/bin/sh

dc(){
  USERID=$(id -u) GROUPID=$(id -g) \
  docker-compose \
    --file dev/docker-compose.yml \
    --project-name tempo-bot \
    "$@"
}
