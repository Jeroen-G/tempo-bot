#!/bin/bash

# include base command
. .Makefile/docker-compose.sh;

clean() {
  grep -v "$1" --line-buffered
}

# set exit code to the last non-zero exit code of any program in the pipeline (if any)
set -o pipefail

dc "$@" 2>&1 |\
    clean "Starting .*" |\
    clean "NOTICE: " |\
    clean "^\[s6-init\] " |\
    clean "^\[fix-attrs\.d\] " |\
    clean "^\[cont-init\.d\] " |\
    clean "^\[services\.d\] " |\
    clean "^\[cont-finish\.d\] " |\
    clean "^\[s6-finish\] " |\
    clean "^\[cmd\] " |\
    clean "^s6-svwait: "
