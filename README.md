# tempo-bot
```
1  There is a time for everything,
   and a season for every activity under the heavens:
2  a time to be born and a time to die,
   a time to plant and a time to uproot,
3  a time to kill and a time to heal,
   a time to tear down and a time to build,

Ecclesiastes 3:1-3
```

A Slack bot that helps you keep an eye on your worked hours, especially of their spread across different projects (or other categories).  

## Requirements
For development:
- bash
- GNU Make
- git
- Docker
- Docker-compose
- Free port 80 (nginx)
- have [hostsupdater](https://github.com/nstapelbroek/hostupdater) running connected to a docker network called `hostsupdater`

For deployment:
- a Gitlab account for the CI/CD pipeline
- a Kubernetes cluster to deploy to
- a (sub-) domain name

## Installation into Slack workspace
- [Botman instructions](https://botman.io/2.0/driver-slack)
- [Slack integration endpoint](tempo-bot.instant-project.johanvo.me)
