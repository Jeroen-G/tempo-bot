
# First command in the file is the default. so please leave this here :-)
info: intro help

# ===========================
# Main commands
# ===========================

help: intro ## This help text
	@awk 'BEGIN {FS = ":.*?[#][#][ ]?"} \
	 /^[a-zA-Z_-]*:?.*?[#][#][ ]?/ {printf "\033[33m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) |\
	 sed --regexp-extended 's/[#]{2,}[ ]*//g'

##
## Project commands
init: intro src/EnvVars/.env do-update-project ## Make the project ready for development.
update: intro do-switch-branch do-update-project ## Update the project, run as 'update BRANCH=<branch>' to checkout a branch first.
clean: intro do-clean ## Cleanup caches etc.

##
## Control dev environment
start: intro do-docker-start ## Start the Docker containers.
stop: intro do-docker-stop ## Stop the Docker containers.
restart: stop start ## Restart the Docker containers.
ngrok: intro do-ngrok ## Start ngrok for the local dev env

##
## Tests
test: intro do-run-tests ## Run the test commands (default none).

##
## Dev commands
logs: intro do-logs ## Tail the logs of all containers.
shell: intro do-shell ## Open a shell in the app container.
builder-shell: intro do-builder-shell ## Open a shell in the builder container.
composer: do-composer ## Run a composer command in the app shell.
monitor: do-monitor ## Monitor the app with lazydocker

# ===========================
# Snippets
# ===========================

dc = .Makefile/dc.sh
dc-clean-output = .Makefile/dc-clean-output.sh
dc-run = ${dc} run -T --rm
dc-run-clean-output = ${dc-clean-output} run -T --rm

# ===========================
# Recipes
# ===========================

do-update-project: do-docker-build do-install-dependencies do-docker-recreate
do-install-dependencies: src/EnvVars/.env do-install-composer-dependencies

# Project commands

src/EnvVars/.env: do-check-software-dependencies conf/.env.dist
	@if [ ! -f "src/EnvVars/.env" ]; then \
  	  	cp -a conf/.env.dist src/EnvVars/.env; \
  	fi

do-check-software-dependencies:
	@if [ ! `command -v git` ]; then \
	    echo "\nPlease install git\n"; \
	    return 1; \
	fi
	@if [ ! `command -v docker` ]; then \
	    echo "\nPlease install docker\n"; \
	    return 1; \
	fi
	@if [ ! `command -v docker-compose` ]; then \
	    echo "\nPlease install docker-compose\n"; \
	    return 1; \
	fi

do-switch-branch:
	@if [ -z $$BRANCH ]; then \
	    echo "\n=== Running updates for the current branch ===\n"; \
	    echo "No branch is set, run: 'make update BRANCH=<branch>' to checkout and update a branch"; \
	else \
	    echo "\n=== Switching to and updating $$BRANCH ===\n"; \
	    git checkout $$BRANCH; \
	    git pull origin $$BRANCH; \
	fi

do-clean: do-docker-stop
	@echo "\n=== Cleaning up caches etc. ===\n"
	@${dc} rm -f
	@rm -rf src/vendor/*

# Run development environment

do-docker-build: src/EnvVars/.env
	@echo "\n=== (Re)building services ===\n"
	@${dc} build $$SERVICE

do-docker-pull: src/EnvVars/.env
	@echo "\n=== Pulling images ===\n"
	@${dc} pull $$SERVICE

do-docker-push: src/EnvVars/.env
	@echo "\n=== Pushing images ===\n"
	@${dc} push $$SERVICE

do-docker-start: src/EnvVars/.env hostsupdater-network
	@echo "\n=== Starting containers ===\n"
	@${dc} up -d --remove-orphans app

do-docker-recreate: src/EnvVars/.env hostsupdater-network
	@echo "\n=== Recreating containers ===\n"
	@${dc} up -d --remove-orphans --force-recreate

do-docker-stop: src/EnvVars/.env
	@echo "\n=== Stopping containers ===\n"
	@${dc} stop

do-install-composer-dependencies: src/EnvVars/.env composer-cache-dir hostsupdater-network
	@echo "\n=== Installing dependencies ===\n"
	@${dc-run-clean-output} app \
		composer --working-dir=/etc/project/src \
		install --prefer-dist --no-suggest

composer-cache-dir:
	@mkdir --parents $${COMPOSER_HOME:-$$HOME/.composer}

hostsupdater-network:
	@if ! docker network inspect hostsupdater > /dev/null 2>&1 ; then \
  	  echo "\n!!! hostsupdater network not found, creating a placeholder !!!\n"; \
	  docker network create --attachable hostsupdater; \
	fi

do-ngrok:
	@docker run --rm -it \
		--net tempo-bot \
		--link tempo-bot_app_1:http \
		wernight/ngrok \
		ngrok http \
		-host-header=dev.tempo-bot.instant-project.johanvo.me \
		app:80

# Tests

do-run-tests:
	@make -s do-run-unit-tests
	@echo "\n\n\n\n\n"
	@make -s do-run-code-coverage

do-run-unit-tests: src/EnvVars/.env hostsupdater-network
	@echo "\n=== Run tests ===\n"
	@${dc-run-clean-output} app src/vendor/bin/phpunit \
		--configuration /etc/project/conf/phpunit.xml --testdox --no-interaction

do-run-code-coverage: src/EnvVars/.env hostsupdater-network
	@echo "\n=== Run code coverage ===\n"
	@${dc-run-clean-output} app src/vendor/bin/phpunit \
		--configuration /etc/project/conf/phpunit.xml --coverage-text

# Dev tools
do-logs:
	@echo "\n=== Watch logs ===\n"
	@${dc} logs -f --tail 5 app redis

do-shell:
	@echo "\n=== Start shell in app container ===\n"
	@${dc} exec app sh

do-builder-shell: src/EnvVars/.env composer-cache-dir
	@echo "\n=== Start shell in builder container ===\n"
	@docker run --rm -it \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v $$PWD:/etc/project \
		registry.gitlab.com/instant-project/tempo-bot/builder:dev \
		/bin/sh

do-composer:
	@echo "\n=== What is the composer command you want to run? ===\n" \
		&& read -p "composer " COMMAND \
		&& echo "\n=== Running \"composer $$COMMAND\" ===\n" \
		&& ${dc-run-clean-output} app composer --working-dir=/etc/project/src $$COMMAND

do-monitor:
	@lazydocker --file dev/docker-compose.yml

# ===========================
# Commands usually the most useful in the CI/CD pipeline
# ===========================

do-docker-login:
	@docker login -u "$$CI_REGISTRY_USER" -p $$CI_JOB_TOKEN $$CI_REGISTRY

IMAGE_TAG:
	@export IMAGE_TAG=$${IMAGE_TAG:=dev}

do-builder-container-build: IMAGE_TAG
	@docker build \
		--file ops/docker/Dockerfile \
		--tag registry.gitlab.com/instant-project/tempo-bot/builder:$${IMAGE_TAG} \
		--build-arg USERID=$${USERID} \
		--build-arg GROUPID=$${GROUPID} \
		--target builder \
		.

do-builder-container-push: IMAGE_TAG
	@docker push registry.gitlab.com/instant-project/tempo-bot/builder:$${IMAGE_TAG}

# ===========================
# Commands needed for the Makefile itself
# ===========================

conf/.env.dist:
	$(error Error: .env.dist not found. Please re-create it or remove all references to it from your Makefile)

.Makefile/headerfile:
	$(error Error: .Makefile/headerfile not found. Please re-create it (by running \
		.Makefile/create_header.sh) or remove all references to it from your Makefile)

intro: .Makefile/headerfile do-check-software-dependencies
	@echo "\033[33m"
	@cat .Makefile/headerfile
	@echo "Dev url:       \033[0m http://dev.tempo-bot.instant-project.johanvo.me/"
	@echo "\n"
