<?php

declare(strict_types=1);

namespace TempoBot\Config\Repository;

use PHPUnit\Framework\TestCase;
use Predis\Client;
use TempoBot\Config\ConfigFactory;
use TempoBot\Config\Exception\InputInvalidException;

class RedisConfigRepositoryTest extends TestCase
{
    /** @var RedisConfigRepository */
    protected $subject;
    private $configFactoryMock;

    public function testIfCurrentUserHasInvalidConfigNullWillBeReturned()
    {
        $this->configFactoryMock
            ->method('build')
            ->willThrowException(new InputInvalidException());

        $config = $this->subject->getCurrentForUser('some_user');

        self::assertNull($config);
    }

    public function setUp(): void
    {
        $this->configFactoryMock = $this->createPartialMock(ConfigFactory::class, ['build']);

        $this->subject = new RedisConfigRepository(
            $this->createMock(Client::class),
            $this->configFactoryMock
        );
    }
}
