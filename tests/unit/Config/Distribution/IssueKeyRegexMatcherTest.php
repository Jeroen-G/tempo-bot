<?php

declare(strict_types=1);

namespace TempoBot\Config\Distribution;

use Carbon\CarbonImmutable;
use PHPUnit\Framework\TestCase;
use TempoBot\Config\Exception\InputInvalidException;
use TempoBot\Tempo\Worklog\Worklog;

class IssueKeyRegexMatcherTest extends TestCase
{
    public function testEmptyPatternsAreNotAllowed()
    {
        $this->expectException(InputInvalidException::class);
        new IssueKeyRegexMatcher('');
    }

    public function testPatternIsIncludedWhenSerializingToJson()
    {
        $matcher = new IssueKeyRegexMatcher('12345');

        $serialized = $matcher->jsonSerialize();

        self::assertSame($matcher->getPattern(), $serialized);
    }

    /**
     * @dataProvider dataProvider
     *
     * @param $pattern
     * @param $issueKey
     * @param $shouldMatch
     */
    public function testMatchesExpectedWorklogs($pattern, $issueKey, $shouldMatch)
    {
        $matcher = new IssueKeyRegexMatcher($pattern);
        $worklog = $this->worklogWithIssueKey($issueKey);

        self::assertSame($shouldMatch, $matcher->matchesWorklog($worklog));
    }

    private function worklogWithIssueKey(string $issueKey): Worklog
    {
        return new Worklog(
            1,
            $issueKey,
            '',
            0,
            0,
            CarbonImmutable::now(),
            CarbonImmutable::now(),
            CarbonImmutable::now()
        );
    }

    public function dataProvider()
    {
        return [
            'project match' => [
                'pattern' => 'TEST-',
                'issueKey' => 'TEST-14',
                'shouldMatch' => true,
            ],
            'single issue match' => [
                'pattern' => 'TEST-14',
                'issueKey' => 'TEST-14',
                'shouldMatch' => true,
            ],
            'catch all match' => [
                'pattern' => '.*',
                'issueKey' => 'TEST-14',
                'shouldMatch' => true,
            ],
            'wrong project nomatch' => [
                'pattern' => 'TEST-',
                'issueKey' => 'OTHER-14',
                'shouldMatch' => false,
            ],
            'single issue nomatch' => [
                'pattern' => 'TEST-14',
                'issueKey' => 'OTHER-14',
                'shouldMatch' => false,
            ],
        ];
    }
}
