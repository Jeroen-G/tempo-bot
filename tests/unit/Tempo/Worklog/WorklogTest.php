<?php

declare(strict_types=1);

namespace TempoBot\Tempo\Worklog;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;

class WorklogTest extends TestCase
{
    public function testFromApiOutputCreatesNewInstance()
    {
        $worklog = Worklog::fromApiOutput($this->getTestInput());

        self::assertInstanceOf(Worklog::class, $worklog);
    }

    protected function getTestInput(): stdClass
    {
        return json_decode(
            file_get_contents(__DIR__ . '/worklog.json')
        );
    }

    /**
     * @param array $overrides
     *
     * @dataProvider invalidInputProvider
     */
    public function testInvalidInputThrowsException(array $overrides)
    {
        $input = $this->getTestInput();
        $this->expectException(InvalidArgumentException::class);

        foreach ($overrides as $key => $value) {
            $input->$key = $value;
        }

        Worklog::fromApiOutput($input);
    }

    public function invalidInputProvider()
    {
        return [
            'tempoWorklogId = null' => [
                'overrides' => [
                    'tempoWorklogId' => null
                ]
            ],
            'description = null' => [
                'overrides' => [
                    'description' => null
                ]
            ],
            'description = int' => [
                'overrides' => [
                    'description' => 14
                ]
            ],
            'timeSpentSeconds = null' => [
                'overrides' => [
                    'timeSpentSeconds' => null
                ]
            ],
            'billableSeconds = null' => [
                'overrides' => [
                    'billableSeconds' => null
                ]
            ],
            'startDate = null' => [
                'overrides' => [
                    'startDate' => null
                ]
            ],
            'startTime = null' => [
                'overrides' => [
                    'startTime' => null
                ]
            ],
            'createdAt = null' => [
                'overrides' => [
                    'createdAt' => null
                ]
            ],
            'updatedAt = null' => [
                'overrides' => [
                    'updatedAt' => null
                ]
            ],
        ];
    }

    public function testEmptyIssueKeyInputThrowsException()
    {
        $input = $this->getTestInput();
        $this->expectException(InvalidArgumentException::class);

        $input->issue->key = '';

        Worklog::fromApiOutput($input);
    }
}
