<?php

declare(strict_types=1);

namespace TempoBot\BotMan\BotMan;

use BotMan\BotMan\BotMan;
use PHPUnit\Framework\TestCase;
use TempoBot\EnvVars\EnvVars;

class BotManFactoryTest extends TestCase
{
    public function testBuild()
    {
        EnvVars::init();

        $botMan = BotManFactory::buildWithHardcodedConfig();

        self::assertIsObject($botMan);
        self::assertInstanceOf(BotMan::class, $botMan);
    }
}
