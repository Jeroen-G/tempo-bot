<?php

declare(strict_types=1);

namespace TempoBot\BotMan\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TempoBot\BotMan\Commands\HelpCommand;
use TempoBot\BotMan\Commands\ReportCommand;
use TempoBot\Config\Config;
use TempoBot\Config\ConfigFactory;

class TempoAccountMiddlewareTest extends TestCase
{
    /** @var callable */
    private $callable;

    /** @var BotMan */
    private $bot;

    /** @var MockObject */
    private $message;

    /** @var TempoAccessMiddleware */
    private $subject;

    /** @var Config */
    private $validConfig;

    public function testWithoutTempoAccountRedirectToTempoHelpCommand()
    {
        $this->messageWithThisConfig(null);
        $this->requestCommand(ReportCommand::getCommandString());
        $this->expectRedirectToTempoHelp(true);

        $this->subject->received($this->message, $this->callable, $this->bot);
    }

    private function messageWithThisConfig($config)
    {
        $this->message
            ->method('getExtras')
            ->with(Config::class)
            ->willReturn($config);
    }

    protected function requestCommand(string $command): void
    {
        $this->message
            ->method('getText')
            ->willReturn($command);
    }

    protected function expectRedirectToTempoHelp(bool $doRedirect): void
    {
        $this->message
            ->expects($doRedirect ? self::once() : self::never())
            ->method('setText')
            ->with('help config invalid');
    }

    public function testNoAccessTokenNeededMeansNoRedirect()
    {
        $this->messageWithThisConfig(null);
        $this->requestCommand(HelpCommand::getCommandString());
        $this->expectRedirectToTempoHelp(false);

        $this->subject->received($this->message, $this->callable, $this->bot);
    }

    public function testUserWithAccessGetsNoRedirect()
    {
        $this->messageWithThisConfig($this->validConfig);
        $this->requestCommand(ReportCommand::getCommandString());
        $this->expectRedirectToTempoHelp(false);

        $this->subject->received($this->message, $this->callable, $this->bot);
    }

    public function testUserWithAccessGetsNoRedirectIfNotNeededAnyway()
    {

        $this->messageWithThisConfig($this->validConfig);
        $this->requestCommand(HelpCommand::getCommandString());
        $this->expectRedirectToTempoHelp(false);

        $this->subject->received($this->message, $this->callable, $this->bot);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->message = $this->createMock(IncomingMessage::class);
        $this->callable = function (IncomingMessage $message) {
            return $message;
        };
        $this->bot = $this->createMock(BotMan::class);

        $this->validConfig = $config = (new ConfigFactory())->build('
{
    "tempoAccountId": "tempo-account-id",
    "tempoAccessToken": "tempo-access-token",
    "distribution": [
        {
            "label": "bucketLabel",
            "matchers": [
                ".*"
            ],
            "plannedHours": 1
        }
    ]
}
'
        );

        $this->subject = new TempoAccessMiddleware();
    }
}
